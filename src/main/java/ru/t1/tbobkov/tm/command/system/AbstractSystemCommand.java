package ru.t1.tbobkov.tm.command.system;

import ru.t1.tbobkov.tm.api.service.ICommandService;
import ru.t1.tbobkov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {


    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
