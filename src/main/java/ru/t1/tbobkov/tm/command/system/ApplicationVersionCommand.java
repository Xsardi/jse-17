package ru.t1.tbobkov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-v";

    public static final String DESCRIPTION = "show version info";

    public static final String NAME = "version";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[version]");
        System.out.println("1.17");
    }

}