package ru.t1.tbobkov.tm.api.repository;

import ru.t1.tbobkov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(Project project);

    void clear();

    Project create(String name, String description);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

    Boolean existsById(String id);

}